$(document).ready(function () {
    $(".products-block .product-grid").hide().slice(0, 9).show();
    if ($(".product-grid:hidden").length != 0) {
        $(".load-more").show();
    }

    $(".load-more").on("click", function (e) {
        e.preventDefault();
        $(".products-block .product-grid:hidden").slice(0, 3).slideDown();
        if ($(".products-block .product-grid:hidden").length == 0) {
            $(".load-more").fadeOut("show");
        }
    });
});

